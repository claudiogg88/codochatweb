<?php

namespace Codo\ChatServerBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServidorControllerTest extends WebTestCase
{
    public function testRegistrar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/registrar');
    }

    public function testVersiones()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/versiones');
    }

    public function testActualizar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/actualizar');
    }

}
