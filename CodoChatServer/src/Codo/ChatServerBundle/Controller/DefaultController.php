<?php

namespace Codo\ChatServerBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this
                ->render('CodoChatServerBundle:Default:index.html.twig',
                        array('name' => $name));
    }
}
