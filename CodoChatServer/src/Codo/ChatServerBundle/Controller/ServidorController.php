<?php

namespace Codo\ChatServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Codo\ChatServerBundle\Entity\Usuario;

class ServidorController extends Controller
{

    public function registrarAction()
    {
        $params = array();
        $content = $this->get("request")->getContent();
        $respuesta = array();
        $response = new JsonResponse();

        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
            $jids = $params["jid"];
            $token = "manuelita";
            if (!$jids)
            {
                $respuesta["error"] = "no llenaste tu papeleo";
            }
            else
            {
                $repository = $this->getDoctrine()->getRepository('CodoChatServerBundle:Usuario');
                $manager = $this->getDoctrine()->getManager();

                $crear = array();
                $creados = array();
                foreach ($jids as $jid)
                {
                    $usuario = $repository->findOneByJid($jid);
                    if (!$usuario)
                    {
                        $crear[] = $jid; //esto es un add
                    }
                    else
                    {
                        $creados[] = $usuario;
                    }
                }
                //caso 1, no tiene cuentas creadas
                if (!$creados)
                {

                }
                //caso 2, tiene uno creado
                if (count($creados) == 1)
                {

                }
                if()
                $usuario = new Usuario();
                $usuario->setJid($jid);
                $usuario->setToken($token);
                $manager->persist($usuario);

                $manager->flush();
                $respuesta["bien"] = "todo bnien";
            }
        }
        $response->setData($respuesta);
        return $response;

    }

    public function versionesAction()
    {

    }

    public function actualizarAction()
    {

    }

}
