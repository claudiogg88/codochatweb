<?php

namespace Codo\ChatServerBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Codo\ChatServerBundle\Entity\Usuario
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Codo\ChatServerBundle\Entity\UsuarioRepository")
 */
class Usuario
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Contacto", inversedBy="usuarios")
     * @ORM\JoinColumn(name="contacto_id", referencedColumnName="id")
     * @return integer
     */
    private $contacto;

    /**
     * @var string $token
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var string $jid
     *
     * @ORM\Column(name="jid", type="string", length=255)
     */
    private $jid;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Usuario
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set jid
     *
     * @param string $jid
     * @return Usuario
     */
    public function setJid($jid)
    {
        $this->jid = $jid;

        return $this;
    }

    /**
     * Get jid
     *
     * @return string 
     */
    public function getJid()
    {
        return $this->jid;
    }

    public function getContacto()
    {
        return $this->contacto;
    }

    public function setContacto(Contacto $contacto)
    {
        $this->contacto = $contacto;
    }

}
