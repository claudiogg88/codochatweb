<?php

namespace Codo\ChatServerBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Codo\ChatServerBundle\Entity\Contacto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Codo\ChatServerBundle\Entity\ContactoRepository")
 */
class Contacto
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct()
    {
        $this->usuarios = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="Usuario", mappedBy="contacto")
     */
    private $usuarios;

    /**
     * @var string $nick
     *
     * @ORM\Column(name="nick", type="string", length=255)
     */
    private $nick;

    /**
     * @var string $subnick
     *
     * @ORM\Column(name="subnick", type="string", length=255)
     */
    private $subnick;

    /**
     * @var string $estado
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var string $perfil
     *
     * @ORM\Column(name="perfil", type="text")
     */
    private $perfil;

    /**
     * @var array $emoticones
     *
     * @ORM\Column(name="emoticones", type="json_array")
     */
    private $emoticones;

    /**
     * @var string $token
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nick
     *
     * @param string $nick
     * @return Contacto
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string 
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set subnick
     *
     * @param string $subnick
     * @return Contacto
     */
    public function setSubnick($subnick)
    {
        $this->subnick = $subnick;

        return $this;
    }

    /**
     * Get subnick
     *
     * @return string 
     */
    public function getSubnick()
    {
        return $this->subnick;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Contacto
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set perfil
     *
     * @param string $perfil
     * @return Contacto
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return string 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set emoticones
     *
     * @param array $emoticones
     * @return Contacto
     */
    public function setEmoticones($emoticones)
    {
        $this->emoticones = $emoticones;

        return $this;
    }

    /**
     * Get emoticones
     *
     * @return array 
     */
    public function getEmoticones()
    {
        return $this->emoticones;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Contacto
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;
    }
    public function addUsuario(Usuario $usuario)
    {
        $this->usuarios[] = $usuario;
    }
}
