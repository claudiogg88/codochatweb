<?php

namespace Codo\ChatServerBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Codo\ChatServerBundle\Entity\Version
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Codo\ChatServerBundle\Entity\VersionRepository")
 */
class Version
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $nick
     *
     * @ORM\Column(name="nick", type="smallint")
     */
    private $nick;

    /**
     * @var integer $subnick
     *
     * @ORM\Column(name="subnick", type="smallint")
     */
    private $subnick;

    /**
     * @var integer $perfil
     *
     * @ORM\Column(name="perfil", type="smallint")
     */
    private $perfil;

    /**
     * @var integer $emoticones
     *
     * @ORM\Column(name="emoticones", type="smallint")
     */
    private $emoticones;

    /**
     * @var integer $jid
     *
     * @ORM\Column(name="jid", type="smallint")
     */
    private $jid;

    /**
     * @var \DateTime $visto
     *
     * @ORM\Column(name="visto", type="datetime")
     */
    private $visto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nick
     *
     * @param integer $nick
     * @return Version
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return integer 
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set subnick
     *
     * @param integer $subnick
     * @return Version
     */
    public function setSubnick($subnick)
    {
        $this->subnick = $subnick;

        return $this;
    }

    /**
     * Get subnick
     *
     * @return integer 
     */
    public function getSubnick()
    {
        return $this->subnick;
    }

    /**
     * Set perfil
     *
     * @param integer $perfil
     * @return Version
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return integer 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set emoticones
     *
     * @param integer $emoticones
     * @return Version
     */
    public function setEmoticones($emoticones)
    {
        $this->emoticones = $emoticones;

        return $this;
    }

    /**
     * Get emoticones
     *
     * @return integer 
     */
    public function getEmoticones()
    {
        return $this->emoticones;
    }

    /**
     * Set jid
     *
     * @param integer $jid
     * @return Version
     */
    public function setJid($jid)
    {
        $this->jid = $jid;

        return $this;
    }

    /**
     * Get jid
     *
     * @return integer 
     */
    public function getJid()
    {
        return $this->jid;
    }

    /**
     * Set visto
     *
     * @param \DateTime $visto
     * @return Version
     */
    public function setVisto($visto)
    {
        $this->visto = $visto;

        return $this;
    }

    /**
     * Get visto
     *
     * @return \DateTime 
     */
    public function getVisto()
    {
        return $this->visto;
    }
}
