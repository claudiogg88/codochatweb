<?php

namespace Codo\ChatServerBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Codo\ChatServerBundle\Entity\Emoticon
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Codo\ChatServerBundle\Entity\EmoticonRepository")
 */
class Emoticon
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $md5
     *
     * @ORM\Column(name="md5", type="string", length=255)
     */
    private $md5;

    /**
     * @var string $imagen
     *
     * @ORM\Column(name="imagen", type="text")
     */
    private $imagen;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return Emoticon
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Emoticon
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }
}
